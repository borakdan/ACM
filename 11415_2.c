#include <stdio.h>
#include <stdlib.h>
#include <inttypes.h>

int array[4000000];
int sum[4000000];
int prime[600];

int main() {
	
	long temp;
	int *sito;
	sito = malloc(2000 * (sizeof(int) + 1));
	long q;
	for (q = 0; q < 2000; q++) sito[q] = 1;
	
	prime[0] = 2;
	int z = 1;
	
	int p;
	for (p = 1; p < 2000; p++)
	{
		if (sito[p] == 1)
		{
			prime[z]=2*p+1;
			z++;
			temp = 3*p+1;
			while (temp <= 2000)
			{
				sito[temp] = 0;
				temp = temp + 2*p + 1;
			}
		}
	}
	free(sito);
	/*printf("%d", z);
	*/
	int i;
	int j;
	
	for (i = 2; i < 3000000; i++) {
		array[i] = 0;
	}
	
	for (i = 2; i < 3000000; i++) {
		int tmp = i;
		for (j = 0; j < z; j++) {
			while (tmp % prime[j] == 0) {
				tmp /= prime[j];
				array[i]++;
			}
			if (tmp == 1) break;
			if (j == z-1 || prime[j] > tmp) array[i]++;
		}
	}	
	
	sum[1] = 0;
	for (i = 2; i < 3000000; i++) {
		sum[i] = sum[i-1] + array[i];
	}
	
	int cases;
	scanf("%d", &cases);
	int target;
	
	while(cases--) {
		scanf("%d", &target);
		for (i = 2; i < 2999999; i++) {
			if (target < sum[i]) {
				printf("%d\n", i);
				break;
			}
		}
		
	}
	
	return 0;
}
