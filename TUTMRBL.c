#include <stdio.h>
#include <math.h>

int main()
{
	int number;
	int prime_number = 2;
	int multiple = 0;
	
	int arr[25];
	int count[25];
	
	while (scanf("%d", &number) == 1)
	{
		prime_number = 2;
		/*numbers = sqrt(number);*/
		int tmp = number;
		if (number == 0)
		{
			return 0;
		}
		int i;
		for (i = 0; i < 25; i++) {
			arr[i] = 0;
			count[i] = 0;
		}
		
		i = 0;
		while (prime_number <= sqrt(number))
		{
			if (number % prime_number == 0)
			{
				number = number / prime_number;
				arr[i] = prime_number;
				if (number % prime_number != 0)
				{
					count[i] = 1;
				}
				else 
				{
					multiple = 1;
					while (number % prime_number == 0)
					{
						number = number / prime_number;
						multiple++;
					}
					count[i] = multiple;
				}
				i++;
			}
			prime_number++;
		}
		if (number != 1) {
			arr[i] = number;
			count[i] = 1;
			i++;
		}
		
		int j;
		int combinations = 1;
		for (j = 0; j < i; j++) {
			combinations *= (count[j]+1);
		}
		printf("%d = ", tmp);
		combinations = (combinations + 1)/2;
		for (j = 0; j < i; j++) {
			while (count[j] > 0) {
				if (j == i - 1 && count[j] == 1) {
					printf("%d\n", arr[j]);
					count[j]--;
				} else {
					printf("%d * ", arr[j]);
					count[j]--;
				}
			}
		}
		
		printf("With %d marbles, %d different rectangles can be constructed.\n", tmp, combinations);
		/*printf("%d\n", number);*/
	}
	return 0;
}
