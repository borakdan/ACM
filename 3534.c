#include <stdio.h>

int arr1[10001];
int arr2[10001];

int ret1;
int ret2;

int next_common_number (int start, int arr1size, int arr2size) {
	int temp = start;
	int j = 0;
	if (temp >= arr1size) {
		return -1;
	}
	while (1) {
		for (j = 0; j < arr2size; j++) {
			if (arr1[temp] == arr2[j]) {
				ret1 = temp;
				ret2 = j;
				return 1;
			}
		}
		temp++;
		if (temp >= arr1size) {
			return -1;
		}
	}
	return -1;
}


int main()
{
	int i = 0;
	int arr1count;
	int arr2count;
	
	int start1 = 0;
	int start2 = 0;
	
	int total = 0;
	int returned = 0;
	
	int temp1total;
	int temp2total;
	
	while(scanf("%d", &arr1count) == 1) {
		if (arr1count == 0) {
			return 0;
		}
		start1 = 0;
		start2 = 0;
		total = 0;
		
		for (i = 0; i < arr1count; i++) {
			scanf("%d", &arr1[i]);
		}
		scanf("%d", &arr2count);
		for (i = 0; i < arr2count; i++) {
			scanf("%d", &arr2[i]);
		}
		
		while(1) {
			returned = next_common_number(start1, arr1count, arr2count);
			temp1total = 0;
			temp2total = 0;
			if (returned == 1) {
				for (i = start1; i <= ret1; i++) {
					temp1total += arr1[i];
				}
				for (i = start2; i <= ret2; i++) {
					temp2total += arr2[i];
				}
				if (temp1total > temp2total) {
					total += temp1total;
				} else total += temp2total;
				start1 = ret1+1;
				start2 = ret2+1;
			}
			if (returned == -1) {
				for (i = start1; i < arr1count; i++) {
					temp1total += arr1[i];
				}
				for (i = start2; i < arr2count; i++) {
					temp2total += arr2[i];
				}
				if (temp1total > temp2total) {
					total += temp1total;
				} else total += temp2total;
				printf("%d\n", total);
				break;
			}
		}
		
	}
	return 0;
}
