#include <stdio.h>
#include <math.h>

int main() {
	long double ln[100005];
	
    int i;
	int a, b, c, d;
    for(i = 1; i <= 100000; i++) {
    	ln[i] = log(i);
	}
        
    while(scanf("%d %d %d %d", &a, &b, &c, &d) == 4) {
        long double result = 0;
        if (a - b < b) {
        	b = a - b;
		}
        if (c - d < d) {
        	d = c - d;
		}
		
        for (i = 1; i <= b; i++) {
        	result += ln[a - b + i] - ln[i];
		}
        for(i = 1; i <= d; i++) {
        	result += - ln[c - d + i] + ln[i];
		}
		
        printf("%.5f\n", exp(result));
    }
    return 0;
}
