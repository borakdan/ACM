#include <stdio.h>
#include <string.h>

int find_number (int x, int bA, int bB, int botA, int botB, int maxClk) {
	int count = 0;
	if (x > 0) {
		while (bA != maxClk && bB != maxClk) {
			if (bA == 0) {
				bA = botA;
				count++;
				continue;
			}
			if (bB == botB) {
				bB = 0;
				count++;
				continue;
			}
			bB += bA;
			bA = 0;
			if (bB > botB) {
				bA = bB - botB;
				bB = botB;
			}
			count++;
			continue;
		}
	} else {
		while (bB != maxClk && bA != maxClk) {
			if (bB == 0) {
				bB = botB;
				count++;
				continue;
			}
			if (bA == botA) {
				bA = 0;
				count++;
				continue;
			}
			bA += bB;
			bB = 0;
			if (bA > botA) {
				bB = bA - botA;
				bA = botA;
			}
			count++;
			continue;
		}
	}
	return count;

}

int main()
{
	int cases;
	int botA, botB, maxClk;

	scanf("%d", &cases);
	
	while(cases--) {
		scanf("%d %d %d", &botA, &botB, &maxClk);
		if (maxClk == botA || maxClk == botB) {
			printf("1\n");
			continue;
		}
		
		if (botA == 0 || botB == 0) {
			printf("-1\n");
			continue;
		}
		
		if (maxClk > botA && maxClk > botB) {
			printf("-1\n");
			continue;
		}
		
		int n1 = botA;
		int n2 = botB;
		while(n1!=n2)
		{
		    if(n1 > n2)
		        n1 -= n2;
		    else
		        n2 -= n1;
		}
		if (maxClk % n1 != 0) {
			printf("-1\n");
			continue;
		}
		int count;
		int temp;
		count = find_number(1, 0, 0, botA, botB, maxClk);
		temp = find_number(-1, 0, 0, botA, botB, maxClk);
		if (temp < count) count = temp;
		
		printf("%d\n", count);
	}
	
	
    return 0;
}
