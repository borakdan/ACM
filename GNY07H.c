#include <stdio.h>


int main()
{
	long long a[50];
	long long b[50];
	long long c[50];
	
	a[0] = 1;
	a[1] = 5;
	b[0] = 1;
	b[1] = 6;
	c[0] = 2;
	c[1] = 7;
	
	int i = 3;
	for (i = 2; i <= 30; i++) {
		a[i] = a[i-1] + a[i-2] + b[i-2] + 2*c[i-2];
		b[i] = a[i] + b[i-2];
		c[i] = a[i] + c[i-1];
		/*printf("%d %lli", i, a[i]);*/
	}
	
	
	int count;
	int num;
	scanf("%d", &count);
	int x = 1;
	while(count--) {
		scanf("%d", &num);
		printf("%d %lli\n", x++, a[num-1]);
	}
	return 0;
}
