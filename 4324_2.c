#include <stdio.h>
#include <string.h>

int main() {
	
	int rows, columns;
	char line[110];
	char arr[110][110];
	int exist[30];
	int top_left[30][2];
	int top_right[30][2];
	int bottom_left[30][2];
	int bottom_right[30][2];
	int bottom_right2[30][2];
	int i, j;
	
	
	
	while(scanf("%d %d", &rows, &columns) == 2) {
		if (rows == 0 || columns == 0) return 0;
		
		for (i = 0; i < 26; i++) {
			exist[i] = 0;
			top_left[i][0] = -1;
			top_left[i][1] = -1;
			top_right[i][0] = -1;
			top_right[i][1] = -1;
			bottom_left[i][0] = -1;
			bottom_left[i][1] = -1;
			bottom_right[i][0] = -1;
			bottom_right[i][1] = -1;
			bottom_right2[i][0] = -1;
			bottom_right2[i][1] = -1;
		}
		
		for (i = 0; i < rows; i++) {
			scanf("%s", &line[0]);
			for (j = 0; j < columns; j++) {
				arr[i][j] = line[j];
				if (line[j] >= 'A' && line[j] <= 'Z') {
					exist[line[j] - 'A']++;
					if (top_left[line[j] - 'A'][0] == -1) {
						top_left[line[j] - 'A'][0] = i;
						top_left[line[j] - 'A'][1] = j;
					}
					
				}
			}
		}
		
		/*
		printf("\n");
		for (i = 0; i < rows; i++) {
			for (j = 0; j < columns; j++) {
				printf("%c", arr[i][j]);
			}
			printf("\n");
		}
		for (i = 0; i < 26; i++) printf("%d ", exist[i]);
		*/
		
		for (i = 0; i < 26; i++) {
			if (exist[i] >= 8 && exist[i] % 2 == 0) {
				int k, m;
				
				for (k = top_left[i][1]; k < columns; k++) {
					if (arr[top_left[i][0]][k] == i + 'A') {
						top_right[i][0] = top_left[i][0];
						top_right[i][1] = k;
					} else break;
				}
				
				for (m = top_left[i][0]; m < rows; m++) {
					if (arr[m][top_left[i][1]] == i + 'A') {
						bottom_left[i][0] = m;
						bottom_left[i][1] = top_left[i][1];
					} else break;
				}
				
				for (k = bottom_left[i][1]; k < columns; k++) {
					if (arr[bottom_left[i][0]][k] == i + 'A') {
						bottom_right[i][0] = bottom_left[i][0];
						bottom_right[i][1] = k;
					} else break;
				}
				
				for (m = top_right[i][0]; m < rows; m++) {
					if (arr[m][top_right[i][1]] == i + 'A') {
						bottom_right2[i][0] = m;
						bottom_right2[i][1] = top_right[i][1];
					} else break;
				}
				/*
				printf("\nLetter %c\n", i + 'A');
				printf("Top Left: %d %d\n", top_left[i][0], top_left[i][1]);
				printf("Top Right: %d %d\n", top_right[i][0], top_right[i][1]);
				printf("Bottom Left: %d %d\n", bottom_left[i][0], bottom_left[i][1]);
				printf("Bottom Right: %d %d\n", bottom_right[i][0], bottom_right[i][1]);
				printf("Bottom Right: %d %d\n", bottom_right2[i][0], bottom_right2[i][1]);
				*/
				if (top_right[i][1] >= top_left[i][1] + 2 && bottom_left[i][0] >= top_left[i][0] + 2
					&& bottom_right[i][0] == bottom_right2[i][0] && bottom_right[i][1] == bottom_right2[i][1]) {
					
					int expected = (bottom_right[i][0] - top_left[i][0] - 1)*(bottom_right[i][1] - top_left[i][1] - 1);
					int result = 0;
					for (k = top_left[i][0] + 1; k < bottom_right[i][0]; k++) {
						for (m = top_left[i][1] + 1; m < bottom_right[i][1]; m++) {
							if (arr[k][m] == '.') result++;
						}
					}
					
					if (expected == result) printf("%c", i+'A');
				}
			}
		}
		printf("\n");
	}
	
	
	
	
	
	
	return 0;
}

