#include <stdio.h>

int delit(int numba) {
	int delitel = 2;
	
	while (numba != 1) {
		while (numba % delitel == 0) {
			if (delitel % 10 != 3) return -1;
			numba /= delitel;
		}
		delitel++;
	}
	return 1;
}

int main()
{
	int number;
	int status;
	
	while(scanf("%d", &number) == 1) {
		if (number == -1) {
			return 0;
		}
		printf("%d ", number);
		
		status = delit(number);
		
		if (status == -1) {
			printf("NO\n");
		}
		if (status == 1) {
			printf("YES\n");
		}
		
	}
	return 0;
}
