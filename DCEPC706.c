#include <stdio.h>
#include <string.h>

int arr[205][205];
int arr1[205][205];
int arr2[205][205];
int arr3[205][205];

int check[205][205];

int stack[41000][2];
int stackmax;
int stackcurrent = 0;

/*
void func(int width, int height) {
	while (stackmax != stackcurrent) {
		int w = stack[stackcurrent][0];
		int h = stack[stackcurrent][1];
		
		if (h > 0) {
			if (arr1[w][h-1] == 200000) {
				if (arr1[w][h-1] > arr1[w][h] + 1) {
					arr1[w][h-1] = arr1[w][h] + 1;
					//func1(width, height, w, h - 1);
					stack[stackmax][0] = w;
					stack[stackmax][1] = h - 1;
					stackmax++;
				}
			}
		}
		if (h <= height) {
			if (arr1[w][h+1] == 200000) {
				if (arr1[w][h+1] > arr1[w][h] + 1) {
					arr1[w][h+1] = arr1[w][h] + 1;
					//func1(width, height, w, h + 1);
					stack[stackmax][0] = w;
					stack[stackmax][1] = h + 1;
					stackmax++;
				}
			}
		}
		if (w > 0) {
			if (arr1[w-1][h] == 200000) {
				if (arr1[w-1][h] > arr1[w][h] + 1) {
					arr1[w-1][h] = arr1[w][h] + 1;
					//func1(width, height, w - 1, h);
					stack[stackmax][0] = w - 1;
					stack[stackmax][1] = h;
					stackmax++;
				}
			}
		}
		if (w <= width) {
			if (arr1[w+1][h] == 200000) {
				if (arr1[w+1][h] > arr1[w][h] + 1) {
					arr1[w+1][h] = arr1[w][h] + 1;
					//func1(width, height, w + 1, h);
					stack[stackmax][0] = w + 1;
					stack[stackmax][1] = h;
					stackmax++;
				}
			}
		}
		stackcurrent++;
	}
}
*/

void func1(int width, int height) {
	while (stackmax != stackcurrent) {
		int w = stack[stackcurrent][0];
		int h = stack[stackcurrent][1];
		
		if (h > 0) {
			if (arr1[w][h-1] == 200000) {
				if (arr1[w][h-1] > arr1[w][h] + 1) {
					arr1[w][h-1] = arr1[w][h] + 1;
					//func1(width, height, w, h - 1);
					stack[stackmax][0] = w;
					stack[stackmax][1] = h - 1;
					stackmax++;
				}
			}
		}
		if (h <= height) {
			if (arr1[w][h+1] == 200000) {
				if (arr1[w][h+1] > arr1[w][h] + 1) {
					arr1[w][h+1] = arr1[w][h] + 1;
					//func1(width, height, w, h + 1);
					stack[stackmax][0] = w;
					stack[stackmax][1] = h + 1;
					stackmax++;
				}
			}
		}
		if (w > 0) {
			if (arr1[w-1][h] == 200000) {
				if (arr1[w-1][h] > arr1[w][h] + 1) {
					arr1[w-1][h] = arr1[w][h] + 1;
					//func1(width, height, w - 1, h);
					stack[stackmax][0] = w - 1;
					stack[stackmax][1] = h;
					stackmax++;
				}
			}
		}
		if (w <= width) {
			if (arr1[w+1][h] == 200000) {
				if (arr1[w+1][h] > arr1[w][h] + 1) {
					arr1[w+1][h] = arr1[w][h] + 1;
					//func1(width, height, w + 1, h);
					stack[stackmax][0] = w + 1;
					stack[stackmax][1] = h;
					stackmax++;
				}
			}
		}
		stackcurrent++;
	}
}

void func2(int width, int height) {
	while (stackmax != stackcurrent) {
		int w = stack[stackcurrent][0];
		int h = stack[stackcurrent][1];
		
		if (h > 0) {
			if (arr2[w][h-1] == 200000) {
				if (arr2[w][h-1] > arr2[w][h] + 1) {
					arr2[w][h-1] = arr2[w][h] + 1;
					//func1(width, height, w, h - 1);
					stack[stackmax][0] = w;
					stack[stackmax][1] = h - 1;
					stackmax++;
				}
			}
		}
		if (h <= height) {
			if (arr2[w][h+1] == 200000) {
				if (arr2[w][h+1] > arr2[w][h] + 1) {
					arr2[w][h+1] = arr2[w][h] + 1;
					//func1(width, height, w, h + 1);
					stack[stackmax][0] = w;
					stack[stackmax][1] = h + 1;
					stackmax++;
				}
			}
		}
		if (w > 0) {
			if (arr2[w-1][h] == 200000) {
				if (arr2[w-1][h] > arr2[w][h] + 1) {
					arr2[w-1][h] = arr2[w][h] + 1;
					//func1(width, height, w - 1, h);
					stack[stackmax][0] = w - 1;
					stack[stackmax][1] = h;
					stackmax++;
				}
			}
		}
		if (w <= width) {
			if (arr2[w+1][h] == 200000) {
				if (arr2[w+1][h] > arr2[w][h] + 1) {
					arr2[w+1][h] = arr2[w][h] + 1;
					//func1(width, height, w + 1, h);
					stack[stackmax][0] = w + 1;
					stack[stackmax][1] = h;
					stackmax++;
				}
			}
		}
		stackcurrent++;
	}
}

void func3(int width, int height) {
	while (stackmax != stackcurrent) {
		int w = stack[stackcurrent][0];
		int h = stack[stackcurrent][1];
		
		if (h > 0) {
			if (arr3[w][h-1] == 200000) {
				if (arr3[w][h-1] > arr3[w][h] + 1) {
					arr3[w][h-1] = arr3[w][h] + 1;
					//func1(width, height, w, h - 1);
					stack[stackmax][0] = w;
					stack[stackmax][1] = h - 1;
					stackmax++;
				}
			}
		}
		if (h <= height) {
			if (arr3[w][h+1] == 200000) {
				if (arr3[w][h+1] > arr3[w][h] + 1) {
					arr3[w][h+1] = arr3[w][h] + 1;
					//func1(width, height, w, h + 1);
					stack[stackmax][0] = w;
					stack[stackmax][1] = h + 1;
					stackmax++;
				}
			}
		}
		if (w > 0) {
			if (arr3[w-1][h] == 200000) {
				if (arr3[w-1][h] > arr3[w][h] + 1) {
					arr3[w-1][h] = arr3[w][h] + 1;
					//func1(width, height, w - 1, h);
					stack[stackmax][0] = w - 1;
					stack[stackmax][1] = h;
					stackmax++;
				}
			}
		}
		if (w <= width) {
			if (arr3[w+1][h] == 200000) {
				if (arr3[w+1][h] > arr3[w][h] + 1) {
					arr3[w+1][h] = arr3[w][h] + 1;
					//func1(width, height, w + 1, h);
					stack[stackmax][0] = w + 1;
					stack[stackmax][1] = h;
					stackmax++;
				}
			}
		}
		stackcurrent++;
	}
}

/*
func1(int width, int height, int w, int h) {
	if (h > 0) {
		if (arr1[w][h-1] >= 0) {
			if (arr1[w][h-1] > arr1[w][h] + 1) {
				arr1[w][h-1] = arr1[w][h] + 1;
				func1(width, height, w, h - 1);
			}
		}
	}
	if (h <= height) {
		if (arr1[w][h+1] >= 0) {
			if (arr1[w][h+1] > arr1[w][h] + 1) {
				arr1[w][h+1] = arr1[w][h] + 1;
				func1(width, height, w, h + 1);
			}
		}
	}
	if (w > 0) {
		if (arr1[w-1][h] >= 0) {
			if (arr1[w-1][h] > arr1[w][h] + 1) {
				arr1[w-1][h] = arr1[w][h] + 1;
				func1(width, height, w - 1, h);
			}
		}
	}
	if (w <= width) {
		if (arr1[w+1][h] >= 0) {
			if (arr1[w+1][h] > arr1[w][h] + 1) {
				arr1[w+1][h] = arr1[w][h] + 1;
				func1(width, height, w + 1, h);
			}
		}
	}
}
*/
/*
func2(int width, int height, int w, int h) {
	if (h > 0) {
		if (arr2[w][h-1] >= 0) {
			if (arr2[w][h-1] > arr2[w][h] + 1) {
				arr2[w][h-1] = arr2[w][h] + 1;
				func2(width, height, w, h - 1);
			}
		}
	}
	if (h <= height) {
		if (arr2[w][h+1] >= 0) {
			if (arr2[w][h+1] > arr2[w][h] + 1) {
				arr2[w][h+1] = arr2[w][h] + 1;
				func2(width, height, w, h + 1);
			}
		}
	}
	if (w > 0) {
		if (arr2[w-1][h] >= 0) {
			if (arr2[w-1][h] > arr2[w][h] + 1) {
				arr2[w-1][h] = arr2[w][h] + 1;
				func2(width, height, w - 1, h);
			}
		}
	}
	if (w <= width) {
		if (arr2[w+1][h] >= 0) {
			if (arr2[w+1][h] > arr2[w][h] + 1) {
				arr2[w+1][h] = arr2[w][h] + 1;
				func2(width, height, w + 1, h);
			}
		}
	}
}

func3(int width, int height, int w, int h) {
	if (h > 0) {
		if (arr3[w][h-1] >= 0) {
			if (arr3[w][h-1] > arr3[w][h] + 1) {
				arr3[w][h-1] = arr3[w][h] + 1;
				func3(width, height, w, h - 1);
			}
		}
	}
	if (h <= height) {
		if (arr3[w][h+1] >= 0) {
			if (arr3[w][h+1] > arr3[w][h] + 1) {
				arr3[w][h+1] = arr3[w][h] + 1;
				func3(width, height, w, h + 1);
			}
		}
	}
	if (w > 0) {
		if (arr3[w-1][h] >= 0) {
			if (arr3[w-1][h] > arr3[w][h] + 1) {
				arr3[w-1][h] = arr3[w][h] + 1;
				func3(width, height, w - 1, h);
			}
		}
	}
	if (w <= width) {
		if (arr3[w+1][h] >= 0) {
			if (arr3[w+1][h] > arr3[w][h] + 1) {
				arr3[w+1][h] = arr3[w][h] + 1;
				func3(width, height, w + 1, h);
			}
		}
	}
}
*/

int main()
{
	int i, j;
	for (i = 0; i < 205; i++) {
		for (j = 0; j < 205; j++) {
			arr[i][j] = 0;
		}
	}
	
	int cases;
	scanf("%d", &cases);
	int width, height;
	char ch;
	
	int w1, h1, w2, h2, w3, h3;
	int min, temp;
	
	while(cases--) {
		scanf("%d %d", &width, &height);
		for (i = 0; i < 205; i++) {
			for (j = 0; j < 205; j++) {
				arr[i][j] = 200000;
			}
		}
		for (i = 1; i <= width; i++) {
			scanf("%c", &ch);
			for (j = 1; j <= height; j++) {
				scanf("%c", &ch);
				if (ch == '#') arr[i][j] = -1;
				if (ch == '1') {
					w1 = i;
					h1 = j;
				}
				if (ch == '2') {
					w2 = i;
					h2 = j;
				}
				if (ch == '3') {
					w3 = i;
					h3 = j;
				}
			}
		}
		for (i = 0; i <= width+1; i++) {
			for (j = 0; j <= height+1; j++) {
				arr1[i][j] = arr[i][j];
				arr2[i][j] = arr[i][j];
				arr3[i][j] = arr[i][j];
			}
		}
		arr1[w1][h1] = 0;
		arr2[w2][h2] = 0;
		arr3[w3][h3] = 0;
		
		/*func1(width, height, w1, h1);
		func2(width, height, w2, h2);
		func3(width, height, w3, h3);*/
		
		stackcurrent = 0;
		stackmax = 1;
		stack[0][0] = w1;
		stack[0][1] = h1;
		func1(width, height);
		
		stackcurrent = 0;
		stackmax = 1;
		stack[0][0] = w2;
		stack[0][1] = h2;
		func2(width, height);
		
		stackcurrent = 0;
		stackmax = 1;
		stack[0][0] = w3;
		stack[0][1] = h3;
		func3(width, height);
		
		/*
		for (i = 1; i <= width; i++) {
			for (j = 1; j <= height; j++) {
				printf("%d\t", arr[i][j]);
			}
			printf("\n");
		}
		*/
		
		
		/*for (i = 0; i <= width+1; i++) {
			for (j = 0; j <= height+1; j++) {
				printf("%d\t", arr1[i][j]);
			}
			printf("\n");
		}
		/*
		for (i = 0; i <= width+1; i++) {
			for (j = 0; j <= height+1; j++) {
				printf("%d\t", arr2[i][j]);
			}
			printf("\n");
		}
		printf("\n");
		for (i = 0; i <= width+1; i++) {
			for (j = 0; j <= height+1; j++) {
				printf("%d\t", arr3[i][j]);
			}
			printf("\n");
		}
		printf("\n");
		*/
		
		min = 1000000;
		for (i = 0; i <= width+1; i++) {
			for (j = 0; j <= height+1; j++) {
				temp = arr1[i][j];
				if (arr2[i][j] > temp) temp = arr2[i][j];
				if (arr3[i][j] > temp) temp = arr3[i][j];
				
				if (temp >= 0) {
					if (temp < min) min = temp;
				} 
			}
		}
		
		printf("%d\n", min);
	}
	
	
    return 0;
}
