#include <stdio.h>
#include <string.h>

int arrX[35][35][35];

int arr[205][205];
int arr1[205][205];
int arr2[205][205];
int arr3[205][205];

int check[205][205];

int stack[41000][3];
int stackmax;
int stackcurrent = 0;

void func1(int width, int height, int depth) {
	while (stackmax != stackcurrent) {
		int w = stack[stackcurrent][0];
		int h = stack[stackcurrent][1];
		int d = stack[stackcurrent][2];
		if (h > 0) {
			if (arrX[w][h-1][d] == 200000) {
				if (arrX[w][h-1][d] > arrX[w][h][d] + 1) {
					arrX[w][h-1][d] = arrX[w][h][d] + 1;
					stack[stackmax][0] = w;
					stack[stackmax][1] = h - 1;
					stack[stackmax][2] = d;
					stackmax++;
				}
			}
		}
		if (h < height-1) {
			if (arrX[w][h+1][d] == 200000) {
				if (arrX[w][h+1][d] > arrX[w][h][d] + 1) {
					arrX[w][h+1][d] = arrX[w][h][d] + 1;
					stack[stackmax][0] = w;
					stack[stackmax][1] = h + 1;
					stack[stackmax][2] = d;
					stackmax++;
				}
			}
		}
		
		if (w > 0) {
			if (arrX[w-1][h][d] == 200000) {
				if (arrX[w-1][h][d] > arrX[w][h][d] + 1) {
					arrX[w-1][h][d] = arrX[w][h][d] + 1;
					stack[stackmax][0] = w - 1;
					stack[stackmax][1] = h;
					stack[stackmax][2] = d;
					stackmax++;
				}
			}
		}
		if (w < width-1) {
			if (arrX[w+1][h][d] == 200000) {
				if (arrX[w+1][h][d] > arrX[w][h][d] + 1) {
					arrX[w+1][h][d] = arrX[w][h][d] + 1;
					stack[stackmax][0] = w + 1;
					stack[stackmax][1] = h;
					stack[stackmax][2] = d;
					stackmax++;
				}
			}
		}
		
		if (d > 0) {
			if (arrX[w][h][d-1] == 200000) {
				if (arrX[w][h][d-1] > arrX[w][h][d] + 1) {
					arrX[w][h][d-1] = arrX[w][h][d] + 1;
					stack[stackmax][0] = w;
					stack[stackmax][1] = h;
					stack[stackmax][2] = d - 1;
					stackmax++;
				}
			}
		}
		if (d < depth-1) {
			if (arrX[w][h][d+1] == 200000) {
				if (arrX[w][h][d+1] > arrX[w][h][d] + 1) {
					arrX[w][h][d+1] = arrX[w][h][d] + 1;
					stack[stackmax][0] = w;
					stack[stackmax][1] = h;
					stack[stackmax][2] = d + 1;
					stackmax++;
				}
			}
		}
		stackcurrent++;
	}
}

int main()
{
	int i, j, k;
	for (i = 0; i < 35; i++) {
		for (j = 0; j < 35; j++) {
			for (k = 0; k < 35; k++) {
				arrX[i][j][k] = 0;
			}
		}
	}
	
	int width, height, depth;
	char ch;
	
	int w1, h1, d1, w2, h2, d2;
	
	while(1) {
		scanf("%d %d %d", &width, &height, &depth);
		if (width == 0 && height == 0 && depth == 0) break;
		for (i = 0; i < 35; i++) {
			for (j = 0; j < 35; j++) {
				for (k = 0; k < 35; k++) {
					arrX[i][j][k] = 200000;
				}
			}
		}
		
		for (i = 0; i < width; i++) {
			scanf("%c", &ch);
			for (j = 0; j < height; j++) {
				for (k = 0; k < depth; k++) {
					scanf("%c", &ch);
					/*if (ch == ' ' || ch == '\0') {
						k--;
						printf("FMLLLLLLLLLLL");
						continue;
					}*/
					if (ch == '#') arrX[i][j][k] = -1;
					if (ch == 'S') {
						w1 = i;
						h1 = j;
						d1 = k;
					}
					if (ch == 'E') {
						w2 = i;
						h2 = j;
						d2 = k;
					}
				}
				scanf("%c", &ch);
			}
		}
		scanf("%c", &ch);
		/*
		for (i = 0; i < width; i++) {
			for (j = 0; j < height; j++) {
				for (k = 0; k < depth; k++) {
					printf("%d\t", arrX[i][j][k]);
				}
				printf("\n");
			}
			printf("\n");
		}*/
		
		for (i = 0; i <= width+1; i++) {
			for (j = 0; j <= height+1; j++) {
				arr1[i][j] = arr[i][j];
				arr2[i][j] = arr[i][j];
				arr3[i][j] = arr[i][j];
			}
		}
		arrX[w1][h1][d1] = 0;
		
		stackcurrent = 0;
		stackmax = 1;
		stack[0][0] = w1;
		stack[0][1] = h1;
		stack[0][2] = d1;
		func1(width, height, depth);
		
		/*
		for (i = 0; i < width; i++) {
			for (j = 0; j < height; j++) {
				for (k = 0; k < depth; k++) {
					printf("%d\t", arrX[i][j][k]);
				}
				printf("\n");
			}
			printf("\n");
		}*/
		
		if (arrX[w2][h2][d2] == 200000) printf("Trapped!\n");
		else printf("Escaped in %d minute(s).\n", arrX[w2][h2][d2]);
	}
	
	
    return 0;
}
