#include <stdio.h>
#include <string.h>

int arr[5020];
long dyn[5020];

void func(int i, int max) {
	if (max == i) {
		dyn[i+1] += dyn[i];
		return;
	}
	if (max - i > 0) {
		if (arr[i] > 2 || arr[i] == 0) {
			dyn[i+1] += dyn[i];
			if (arr[i+1] == 0) {
				dyn[i+1] = 0;
			}
		}
		
		if (arr[i] == 1) {
			if (arr[i+1] == 0) {
				dyn[i+1] += dyn[i];
			}
			
			if (arr[i+1] > 2) {
				dyn[i+1] += dyn[i];
				dyn[i+2] += dyn[i];
			}
			
			if (arr[i+1] == 1) {
				if (arr[i+2] == 0) {
					dyn[i+1] += dyn[i];
				} else {
					dyn[i+1] += dyn[i];
					dyn[i+2] += dyn[i];
				}
			}
			
			if (arr[i+1] == 2) {
				if (arr[i+2] == 0) {
					dyn[i+1] += dyn[i];
				} else if (arr[i+2] < 7) {
					dyn[i+1] += dyn[i];
					dyn[i+2] += dyn[i];
				}
				else {
					dyn[i+1] += dyn[i];
				}
			}
		}
		
		if (arr[i] == 2) {
			if (arr[i+1] == 0) {
				dyn[i+1] += dyn[i];
			}
			
			if (arr[i+1] > 2) {
				if (arr[i+1] > 6) {
					dyn[i+1] += dyn[i];
				} else {
					dyn[i+1] += dyn[i];
					dyn[i+2] += dyn[i];
				}
			}
			
			if (arr[i+1] == 1) {
				if (arr[i+2] == 0) {
					dyn[i+1] += dyn[i];
				} else {
					dyn[i+1] += dyn[i];
					dyn[i+2] += dyn[i];
				}
			}
			
			if (arr[i+1] == 2) {
				if (arr[i+2] == 0) {
					dyn[i+1] += dyn[i];
				} else if (arr[i+2] < 7) {
					dyn[i+1] += dyn[i];
					dyn[i+2] += dyn[i];
				}
				else {
					dyn[i+1] += dyn[i];
				}
			}
		}
		func(i+1, max);
	}
}

int main()
{
	int i = 0;
	int j;
	for (j = 0; j < 5010; j++) {
		arr[j] = 0;
		dyn[j] = 0;
	}
	
	
	while(1) {
		scanf("%c", &arr[i]);
		arr[i] -= '0';
		if (i == 0 && arr[i] == 0) {
			return 0;
		}
		if (arr[i] < 0 || arr[i] > 9) {
			dyn[0] = 1;
			func(0, i-1);
			printf("%ld\n", dyn[i]);
			i = 0;
			for (j = 0; j < 5020; j++) {
				arr[j] = 0;
				dyn[j] = 0;
			}
			continue;
		}
		i++;
	}
	
	
    return 0;
}
