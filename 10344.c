#include <stdio.h>
#include <string.h>

int complete = -1;

int add(int a, int b) {
	return a+b;
}

int sub(int a, int b) {
	return a-b;
}

int mul(int a, int b) {
	return a*b;
}

void set2(int a, int b) {
	if (complete != -1) return;
	if (add(a,b) == 23 || sub(a,b) == 23 || mul(a,b) == 23) {
		complete = 1;
	}
}

void set3(int a, int b, int c) {
	if (complete != -1) return;
	set2(add(a,b), c);
	set2(sub(a,b), c);
	set2(mul(a,b), c);
}

void set4(int a, int b, int c, int d) {
	if (complete != -1) return;
	set3(add(a,b), c, d);
	set3(sub(a,b), c, d);
	set3(mul(a,b), c, d);
}

void set5(int a, int b, int c, int d, int e) {
	set4(add(a,b), c, d, e);
	set4(sub(a,b), c, d, e);
	set4(mul(a,b), c, d, e);
}

void swap(int *array, int a, int b) {
	int tmp = array[a];
	array[a] = array[b];
	array[b] = tmp;
	return;
}


void permute(int *array,int i,int length) { 
	if (length == i) {
		if (complete != -1) return;
			set5(array[0], array[1], array[2], array[3], array[4]);
			return;
		}
	int j = i;
	for (j = i; j < length; j++) { 
		swap(array, i, j);
		permute(array,i+1,length);
		swap(array, i, j);
	}
	return;
}








int main()
{
	int arr[5];
	
	while(1) {
		complete = -1;
		scanf("%d %d %d %d %d", &arr[0], &arr[1], &arr[2], &arr[3], &arr[4]);
		if (arr[0] == 0 && arr[1] == 0 && arr[2] == 0 && arr[3] == 0 && arr[4] == 0) {
			break;
		}
		
		permute(arr, 0, 5);
		
		if (complete == 1) {
			printf("Possible\n");
		} else printf("Impossible\n");
	}
	
	
    return 0;
}
