#include <stdio.h>


int main()
{
	int count;
	int num;
	scanf("%d", &count);
	int x = 1;
	
	int i;
	int a[4];
	int b[4];
	double r;
	double area;
	
	double one, two;
	double oneslashtwo;
	
	while(count--) {
		for (i = 0; i < 4; i++) {
			scanf("%d", &a[i]);
		}
		for (i = 0; i < 4; i++) {
			scanf("%d", &b[i]);
		}
		scanf("%lf", &r);
		one = 30.0 * a[0] + 30.0/60.0 * a[1] + 30.0/60.0/60.0 * a[2] + 30.0/60.0/60.0/100.0 * a[3];
		two = 30.0 * b[0] + 30.0/60.0 * b[1] + 30.0/60.0/60.0 * b[2] + 30.0/60.0/60.0/100.0 * b[3];
		oneslashtwo = one - two;
		if (oneslashtwo < 0) oneslashtwo = - oneslashtwo;
		/*printf("%.3lf %.3lf\n", oneslashtwo);
		printf("%.3lf %.3lf\n", one, two);
		printf("R = %.3lf\n", r);*/
		area = 3.14159265*r*r * oneslashtwo / 360.0;
		
		printf("%d. %.3f\n", x++, area);
	}
	return 0;
}
