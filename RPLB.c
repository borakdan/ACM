#include <stdio.h>

int DP[1010][1010];
int arr[1010];
int items, capacity;

int give(int i, int total)
{
    if (i >= items) return 0;
    if (DP[i][total] != -1) return DP[i][total];
    
    int first = 0;
	int second = 0;
	
    if (arr[i] + total <= capacity) first = arr[i] + give(i + 2, total + arr[i]);
    second = give(i + 1, total);
    
	if (first > second) DP[i][total] = first;
    else DP[i][total] = second;
	
    return DP[i][total];
}

int main()
{
    int cases;
	int count = 1;
    scanf("%d", &cases);
    
    int i, j;
    
    while(cases--)
    {
    	for (i = 0; i < 1010; i++) {
    		for (j = 0; j < 1010; j++) {
    			DP[i][j] = -1;
			}
			arr[i] = 0;
		}
        scanf("%d %d", &items, &capacity);
        for(i=0; i < items; i++)
        {
            scanf("%d", &arr[i]);
        }
        int ans = give(0,0);
        printf("Scenario #%d: %d\n", count++, ans);
    }
    return 0;
}
