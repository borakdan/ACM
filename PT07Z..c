#include <stdio.h>
#include <string.h>

int cases = -4;
int max_node = -1;
int max = -1;
int start[10005];
int end[10005];
int visited[10005];



void DFS(int node, int i) {
	if (i > max) {
		max_node = node;
		max = i;
	}
    visited[node]=1;
    int j;
    for (j = 1; j < cases; j++) {
    	if (start[j] == node) {
    		if (visited[end[j]] == 0) {
    			visited[end[j]] = 1;
				DFS(end[j], i+1);
			}
		} else if (end[j] == node) {
    		if (visited[start[j]] == 0) {
    			visited[start[j]] = 1;
				DFS(start[j], i+1);
			}
		}
	}
}

int main() {
	scanf("%d", &cases);
	
	int k;
	for(k = 1; k < cases; k++) {
		scanf("%d %d", &start[k], &end[k]);
		visited[k] = 0;
	}
	
	DFS(1, 0);
	for(k = 1; k < cases; k++) {
		visited[k] = 0;
	}
	DFS(max_node, 0);
	printf("%d\n", max);
	
    return 0;
}
