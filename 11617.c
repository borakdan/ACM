#include <stdio.h>




int arr[1024][1024];
int left[1024];
int right[1024];
int DP[1024][2];

int absolute(int a, int b) {
	if (a - b >= 0) return a -b;
	else return b - a;
}

int min(int a, int b) {
	if (a <= b) return a;
	else return b;
}

int main() {
	
	
	int cases;
	scanf("%d", &cases);
	int row, column;
	int i, j;
	char tmp;
	
	int even_check = 0;
	int max_rows = 0;
	
	while(cases--) {
		scanf("%d %d", &column, &row);
		
		for (i = 0; i < row; i++) {
			for (j = 0; j < column; j++) {
				while (1) {
					scanf("%c", &tmp);
					tmp -= '0';
					if (tmp > 0 && tmp < 10) {
						arr[i][j] = tmp;
						break;
					}
				}
			}
		}
		
		for (i = 0; i < row; i++) {
			left[i] = -1;
			right[i] = -1;
			DP[i][0] = -1;
			DP[i][1] = -1;
		}
		
		even_check = 0;
		for (i = 0; i < row; i++) {
			for (j = 0; j < column; j++) {
				if (arr[i][j] % 2 == 0) {
					even_check++;
					right[i] = j;
					if (left[i] == -1) {
						left[i] = j;
					}
				}
			}
		}
		
		if (even_check == 0) {
			printf("0\n");
			continue;
		}
		max_rows = 0;
		
		for (j = row - 1; j >= 0; j--) {
			if (left[j] != -1) {
				max_rows = j;
				break;
			}
		}
		
		int pivot[2];
		pivot[0] = 0;
		pivot[1] = 0;
		DP[0][0] = 0;
		DP[0][1] = 0;
		
		if (left[0] != -1) {
			pivot[0] = right[0];
			pivot[1] = right[0];
			DP[0][0] = right[0];
			DP[0][1] = right[0];
		}
		/*printf("row 0, %d %d\n", DP[0][0], DP[0][1]);*/
		for (j = 1; j <= max_rows; j++) {
			if (left[j] != -1) {
				int t1 = absolute(pivot[0], right[j]) + right[j] - left[j] + DP[j-1][0];
				int t2 = absolute(pivot[1], right[j]) + right[j] - left[j] + DP[j-1][1];
				DP[j][0] = min(t1, t2);
				
				t1 = absolute(pivot[0], left[j]) + right[j] - left[j] + DP[j-1][0];
				t2 = absolute(pivot[1], left[j]) + right[j] - left[j] + DP[j-1][1];
				DP[j][1] = min(t1, t2);
				
				pivot[0] = left[j];
				pivot[1] = right[j];
			} else {
				DP[j][0] = DP[j-1][0];
				DP[j][1] = DP[j-1][1];
			}
			/*printf("row %d, %d %d, left+right %d %d\n", j, DP[j][0], DP[j][1], left[j], right[j]);*/
		}
		int result = min(DP[max_rows][0], DP[max_rows][1]) + max_rows;
		printf("%d\n", result);
	}
}
