#include <stdio.h>

int arr1[10001];
int arr2[10001];

int ret1;
int ret2;

int next_common_number (int start, int arr1size, int arr2size) {
	int temp = start;
	int j = 0;
	if (temp >= arr1size) {
		return -1;
	}
	while (1) {
		for (j = 0; j < arr2size; j++) {
			if (arr1[temp] == arr2[j]) {
				ret1 = temp;
				ret2 = j;
				return 1;
			}
		}
		temp++;
		if (temp >= arr1size) {
			return -1;
		}
	}
	return -1;
}


int main()
{
	long long a[31];
	long long b[31];
	
	a[0] = 1;
	a[1] = 0;
	b[0] = 0;
	b[1] = 1;
	
	int i = 2;
	for (i = 2; i <= 30; i++) {
		a[i] = a[i-2] + 2*b[i-1];
		b[i] = a[i-1] + b[i-2];
	}
	
	while(scanf("%d", &i) == 1) {
		if (i == -1) {
			return 0;
		}
		printf("%lli\n", a[i]);
	}
	return 0;
}
