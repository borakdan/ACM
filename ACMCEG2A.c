#include <stdio.h>
#include <string.h>

char number[20];

void swapping(int len, int swap) {
	if (len < 2) return;
	
	int i, j, k, l;
	int loc = 0;
	
	char tmp = 'a';
	
	char min = 'z';
	
	while (swap--) {
		min = 'z';
		loc = len;
		
		for (j = len-1; j >= 0; j--) {
			if (number[j] < number[j+1]) {
				break;
			}
		}
		
		
		for (i = j+1; i <= len; i++) {
			if (number[i] <= min && number[i] > number[j]) {
				min = number[i];
				loc = i;
			}
		}
		
		tmp = number[j];
		number[j] = number[loc];
		number[loc] = tmp;
		
		for (k = j+1; k <= len; k++) {
			for (l = k+1; l <= len; l++) {
				if (number[k] > number[l]) {
					tmp = number[k];
					number[k] = number[l];
					number[l] = tmp;
				}
			}
		}
	}
}





int main() {
	int cases;
	int len;
	int swap;
	
	scanf("%d", &cases);
	
	while(cases--) {
		scanf("%s %d", &number[0], &swap);
		len = strlen(number);
		swapping(len-1, swap);
		printf("%s\n", number);
		if (cases != 1) printf("\n");
	}
	
	return 0;
}
